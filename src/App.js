import { Container, Grid, ButtonGroup, Button } from "@mui/material";
import { useState } from "react";
import Login from "./components/Login";
import SignUp from "./components/SignUp";

function App() {
  const [isLogin, setIsLogin] = useState(true);
  return (
    <Container>
      <Grid container style={{display:"flex", justifyContent:"center", height:"100vh"}}>
          <Grid item xs={12} sm={12} md={6} lg={6} mt={3} padding={5} sx={{backgroundColor:"#C0BCB9"}}>
            <ButtonGroup variant="contained" fullWidth mt={5} aria-label="outlined primary button group">
              <Button color={isLogin ? "inherit" : "success"} onClick={()=>setIsLogin(false)}>Sign Up</Button>
              <Button color={isLogin ? "success" : "inherit"} onClick={()=>setIsLogin(true)}>Login</Button>
            </ButtonGroup>
            {
              isLogin ? <Login/> : <SignUp/>
            }
          </Grid>
      </Grid>
    </Container>
  );
}

export default App;
