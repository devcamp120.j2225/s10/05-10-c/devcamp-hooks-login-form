import { Grid, Typography, TextField, Button } from "@mui/material";
import { useState } from "react";

function Login() {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    return (
        <Grid container mt={3}sx={{justifyContent:"center", alignItems:"center"}}>
            <Grid item xs={12} sm={12} md={12} lg={12}>
                <Typography variant="h3" style={{textAlign:"center"}} component="div">
                    Welcome Back!
                </Typography>
            </Grid>
            <Grid item xs={12} sm={12}md={12} lg={12} mt={2}>
                <TextField id="inp-username" fullWidth  label="Email address*" variant="outlined" onChange={(event)=>setUsername(event.target.value)} value={username} />
            </Grid>
            <Grid item xs={12} sm={12}md={12} lg={12} mt={2}>
                <TextField id="inp-password" fullWidth  label="Password*" variant="outlined" onChange={(event)=>setPassword(event.target.value)} value={password} />
            </Grid>  
            <Grid item xs={12} sm={12}md={12} lg={12} mt={2}>
                <Button id="btn-login" fullWidth color="success" variant="contained">Log In</Button>    
            </Grid>          
        </Grid>
    );
}

export default Login;