import { Grid, Typography, TextField, Button } from "@mui/material";
import { useState } from "react";

function SignUp() {
    const [firstname, setFirstname] = useState("");
    const [lastname, setLastname] = useState("");
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    return (
        <Grid container mt={3} sx={{justifyContent:"center", alignItems:"center"}}>
            <Grid item xs={12} sm={12} md={12} lg={12}>
                <Typography variant="h3" style={{textAlign:"center"}} component="div">
                    Sign Up for Free!
                </Typography>
            </Grid>
            <Grid item xs={12} sm={12} md={6} lg={6} mt={2} paddingRight={1}>
                <TextField id="inp-firstname" fullWidth  label="First Name*" variant="outlined" onChange={(event)=>setFirstname(event.target.value)} value={firstname} />
            </Grid>
            <Grid item xs={12} sm={12}md={6} lg={6} mt={2} paddingLeft={1}>
                <TextField id="inp-lastname" fullWidth  label="Last Name*" variant="outlined" onChange={(event)=>setLastname(event.target.value)} value={lastname} />
            </Grid>                        
            <Grid item xs={12} sm={12}md={12} lg={12} mt={2}>
                <TextField id="inp-username" fullWidth  label="Email address*" variant="outlined" onChange={(event)=>setUsername(event.target.value)} value={username} />
            </Grid>
            <Grid item xs={12} sm={12}md={12} lg={12} mt={2}>
                <TextField id="inp-password" fullWidth  label="Set A Password*" variant="outlined" onChange={(event)=>setPassword(event.target.value)} value={password} />
            </Grid>  
            <Grid item xs={12} sm={12}md={12} lg={12} mt={2}>
                <Button id="btn-signup" fullWidth color="success" variant="contained">Get Started</Button>    
            </Grid>          
        </Grid>
    );
}

export default SignUp;